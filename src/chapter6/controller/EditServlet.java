package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		String editId = request.getParameter("id");
		Message message = null;

		if (editId.matches("^[0-9]+$")) {
			int messageId = Integer.parseInt(editId);
			message = new MessageService().select(messageId);
		}

		if (message == null) {
			HttpSession session = request.getSession();

			String errorMessage = "不正なパラメータが入力されました";
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("./");
			return;
		}

        request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("id")));
        message.setText(text);

        if (isValid(text, errorMessages)) {
        	new MessageService().update(message);
        }

        if (errorMessages.size() != 0) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("message", message);
        	request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }
        response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
